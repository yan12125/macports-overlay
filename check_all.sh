set -e

nvchecker -l debug nvchecker.ini

rm -f local_ver.txt
while read -r pkg ; do
    pkgver=$(port -q info --version $pkg)
    echo "$pkg $pkgver" >> local_ver.txt
done < <(cut -d' ' -f 1 newver.txt)

diff -u local_ver.txt newver.txt

echo All packages are up-to-date!
